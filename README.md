# Global Superstore Analysis on Tableau


- Understanding end to end project requirement from the mentor. Creating the detail project plan with milestones and timelines to achieve each of the milestones. Worked on data collation, cleansing and preparation using excel. Ideated and identified lag and lead indicators for the business. Design and develop Tableau BI dashboard to show overall business performance of Global Superstore across geography.
